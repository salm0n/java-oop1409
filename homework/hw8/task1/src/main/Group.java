package main;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class Group {
    private String groupName;
    private final Student[] students = new Student[10];

    public Group(String groupName){
        super();
        this.groupName = groupName;
    }

    public Group(){
        super();
    }

    public String getGroupName(){
        return groupName;
    }

    public void setGroupName(String groupName){
        this.groupName = groupName;
    }

    public Student[] getStudents() {
        return students;
    }

    public void addStudent(Student student) throws GroupOverflowException{
        if (!studentExist(student)){
            for (int i = 0; i < students.length; i++){
                if (students[i] == null){
                    students[i] = student;
                    return;
                }
            }
            throw new GroupOverflowException();
        }
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException{
        for (Student st : students){
            if (st != null){
                if (st.getLastName().equals(lastName)){
                    return st;
                }
            }
        }
        throw new StudentNotFoundException();
    }

    public boolean removeStudentById(int id){
        for (int i = 0; i < students.length; i++){
            if (students[i] != null){
                if (students[i].getId() == id){
                    students[i] = null;
                    return true;
                }
            }
        }
        return false;
    }

    private String sortStudentsByLastName(){
        Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));
        String rez = "";

        for(Student st : students){
            if (st != null){
                rez += st + System.lineSeparator();
            }
        }
        return rez;
    }

    private boolean studentExist(Student student){
        for (Student st : students){
            if (st != null && st.equals(student)){
                System.out.println("Student " + student.getName() + " " + student.getLastName() + " is already in the group");
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "group " + groupName + System.lineSeparator() + sortStudentsByLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(groupName, group.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName);
    }
}
