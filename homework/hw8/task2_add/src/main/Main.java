package main;

public class Main {
    public static void main(String[] args) {
        ArrayBasedStack stack = new ArrayBasedStack();

        stack.push("Hello");
        stack.push("Java");
        stack.push("number");
        stack.push(1);

        System.out.println(stack);
        for (;stack.peek() != null;){
            System.out.println(stack.pop());
        }
        System.out.println(stack);
    }
}
