package main;

import java.util.Arrays;

public class ArrayBasedStack {
    private Object[] dataArr;
    private int size;
    private int capacity;
    private final int DEFAULT_CAPACITY = 16;
    private final int MAX_STACK_SIZE = Integer.MAX_VALUE - 1;

    public ArrayBasedStack(){
        dataArr = new Object[DEFAULT_CAPACITY];
        capacity = dataArr.length;
        size = 0;
    }

    public void push (Object el){
        if (size >= capacity){
            boolean resizeRes = upResize();
            if (!resizeRes){
                throw new RuntimeException("Cannot add element");
            }
        }
        dataArr[size] = el;
        size += 1;
    }

    public Object pop(){
        if (size != 0){
            size -= 1;
            Object tmp = dataArr[size];
            dataArr[size] = null;
            return tmp;
        }
        return null;
    }

    public boolean upResize (){
        if (capacity < MAX_STACK_SIZE){
            long newCapacityL = (capacity * 3L) / 2L + 1L;
            int newCapacity = (newCapacityL < MAX_STACK_SIZE)? (int) newCapacityL : MAX_STACK_SIZE;
            dataArr = Arrays.copyOf(dataArr, newCapacity);
            capacity = newCapacity;
            return true;
        }
        return false;
    }

    public Object peek (){
        if (size != 0){
            return dataArr[size - 1];
        }
        return null;
    }

    public void trimToSize(){
        dataArr = Arrays.copyOf(dataArr, size);
        capacity = dataArr.length;
    }

    public void clear(){
        dataArr = new Object[DEFAULT_CAPACITY];
        capacity = dataArr.length;
        size = 0;
    }

    @Override
    public String toString (){
        String res = "[ ";
        for (int i = 0; i < size; i++) {
            if (i < size - 1){
                res += dataArr[i] + ", ";
            }else{
                res += dataArr[i];
            }
        }
        return res + "]";
    }
}
