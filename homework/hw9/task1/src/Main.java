import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Main {
    public static void main(String[] args) {
        createList();
    }

    public static void createList() {
        List<Integer> li = new ArrayList<>();
        Random rd = new Random();

        for (int i = 0; i < 10; i++) {
            li.add(rd.nextInt(10));
        }

        System.out.println(li);
        System.out.println(li.remove(0));
        System.out.println(li.remove(0));
        System.out.println(li.remove(li.size() - 1));
        System.out.println(li);
    }
}