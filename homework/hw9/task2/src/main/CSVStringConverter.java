package main;

public class CSVStringConverter implements StringConverter{
    @Override
    public String toStringRepresentation(Student student) {

        return student.getName() + "," + student.getLastName() + ","
                + student.getGender() + "," + student.getId() + "," + student.getGroupName();
    }

    @Override
    public Student fromStringRepresentation(String str){
        String[] strArr = str.split(",");
        Student st = new Student();

        for (int i = 0; i < strArr.length; i++){
            st.setName(strArr[0]);
            st.setLastName(strArr[1]);
            st.setGender(Gender.valueOf(strArr[2]));
            st.setId(Integer.valueOf(strArr[3]));
            st.setGroupName(strArr[4]);
        }

        return st;
    }
}
