package main;

import java.io.*;

public class CopyFileService {
    public static long copyAllFiles(File folderIn, File folderOut, String ext) throws IOException {
        File[] files = folderIn.listFiles();
        long totalByte = 0;
        if (files != null) {
            for (File fl : files) {
                File fileOut = new File(folderOut, fl.getName());
                if (fl.getAbsolutePath().endsWith(ext) && fl.isFile()) {
                    totalByte += copyFile(fl, fileOut);
                }
            }
        }
        return totalByte;
    }

    public static long copyFile(File flIn, File flOut) throws IOException{
        try (InputStream is = new FileInputStream(flIn);
             OutputStream os = new FileOutputStream(flOut)) {
            return is.transferTo(os);
        }
    }
}
