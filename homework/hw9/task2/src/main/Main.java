package main;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args){

        Group group1 = new Group("F-31");

        Student st1 = new Student("Harry", "Palmer", Gender.FEMALE, 3, "F-31");
        Student st2 = new Student("Mark", "Barnett", Gender.MALE, 5, "F-31");
        Student st3 = new Student("Lora", "Walters ", Gender.FEMALE, 6, "F-31");
        Student st4 = new Student("Jam", "Bryant", Gender.MALE, 7, "F-31");
        Student st5 = new Student("Dmitriy", "Jackson", Gender.MALE, 9, "F-31");
        Student st6 = new Student("Betty", "Armstrong", Gender.FEMALE, 13, "F-31");
        Student st7 = new Student("John ", "Prokopenko", Gender.MALE, 16, "F-31");
        Student st8 = new Student("Lloyd", "Brooks", Gender.MALE, 14, "F-31");
        Student st9 = new Student("Betty", "Wood", Gender.FEMALE, 11, "F-31");
        Student st10 = new Student("Antonia", "Summers", Gender.FEMALE, 1, "F-31");
        Student st11 = new Student("Mark", "Barnett", Gender.MALE, 5, "F-31");
        Student st12 = new Student("Mark", "Barnett", Gender.MALE, 5, "F-31");


        try{
            group1.addStudent(st1);
            group1.addStudent(st2);
            group1.addStudent(st3);
            group1.addStudent(st4);
            group1.addStudent(st5);
            group1.addStudent(st6);
            group1.addStudent(st7);
            group1.addStudent(st8);
            group1.addStudent(st9);
            group1.addStudent(st10);
            group1.addStudent(st11);
        }catch (GroupOverflowException e){
            e.printStackTrace();
        }

        System.out.println(group1.removeStudentById(5));
        group1.removeStudentById(1);
        group1.removeStudentById(11);
        System.out.println(group1);


        try{
            GroupFileStorage.saveGroupToCSV(group1);
        }catch (IOException e){
            e.printStackTrace();
        }

        File flGroup = new File("group_list/F-31.csv");
        try{
            System.out.println(GroupFileStorage.loadGroupFromCSV(flGroup));
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(GroupFileStorage.findFileByGroupName("F-31", new File("group_list")));
    }
}
