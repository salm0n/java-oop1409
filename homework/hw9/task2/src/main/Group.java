package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Group {
    private String groupName;
    private final int INITIAL_CAPACITY = 10;
    private final List<Student> students = new ArrayList<>(INITIAL_CAPACITY);

    public Group(String groupName){
        super();
        this.groupName = groupName;
    }

    public Group(){
        super();
    }

    public String getGroupName(){
        return groupName;
    }

    public void setGroupName(String groupName){
        this.groupName = groupName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student) throws GroupOverflowException{
        if (!studentExist(student)){
            if (students.size() < INITIAL_CAPACITY){
                students.add(student);
                return;
            }
            throw new GroupOverflowException();
        }
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException{
        for (Student st : students){
            if (st.getLastName().equals(lastName)) {
                return st;
            }
        }
        throw new StudentNotFoundException();
    }

    public boolean removeStudentById(int id){
        for (Student st : students) {
            if (st.getId() == id){
                students.remove(st);
                return true;
            }
        }
        return false;
    }

    private String sortStudentsByLastName(){
        students.sort(new StudentLastNameComparator());
        String rez = "";

        for(Student st : students){
            rez += st + System.lineSeparator();
        }
        return rez;
    }

    private boolean studentExist(Student student){
        for (Student st : students){
            if (st.equals(student)){
                System.out.println("Student " + student.getName() + " " + student.getLastName() + " is already in the group!");
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "group " + groupName + System.lineSeparator() + sortStudentsByLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(groupName, group.groupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupName);
    }
}