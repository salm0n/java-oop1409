package main;

import java.util.Comparator;

public class StudentLastNameComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Student st1 = (Student) o1;
        Student st2 = (Student) o2;

        String studentLastName1 = st1.getLastName();
        String studentLastName2 = st2.getLastName();

        if (studentLastName1.toLowerCase().compareTo(studentLastName2.toLowerCase()) > 0){
            return 1;
        }
        if (studentLastName1.toUpperCase().compareTo(studentLastName2.toLowerCase()) < 0){
            return -1;
        }
        return 0;
    }
}
