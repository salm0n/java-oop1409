package main;

public class Cat extends Animal{
    private String name;

    public Cat(String ration, String color, int weight, String name){
        super(ration, color, weight);
        this.name = name;
    }

    public Cat() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoice(){
        return "meow";
    }

    public void eat(){
        System.out.println(name + " is eating");
    }

    public void sleep(){
        System.out.println(name + " is sleeping");
    }

    @Override
    public String toString(){
        return "cat [" + "name: " + name + ", " + super.toString() + "]";
    }
}
