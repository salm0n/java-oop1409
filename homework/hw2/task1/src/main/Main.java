package main;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("whiskas", "Black", 3, "Sam");
        Dog dog1 = new Dog("sausage", "Brown", 10, "Jimmy");
        Dog dog2 = new Dog("grass", "Grey", 8, "Jax");

        Veterinarian vet1 = new Veterinarian("Jam");
        Veterinarian vet2 = new Veterinarian("Lora");
        Veterinarian vet3 = new Veterinarian("Dina");

        dog1.eat();
        cat1.sleep();
        System.out.println(cat1.getVoice());

        vet1.treatment(cat1);
        vet2.treatment(dog1);
        vet3.treatment(dog2);

    }
}
