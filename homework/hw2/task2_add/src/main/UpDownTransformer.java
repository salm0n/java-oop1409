package main;

public class UpDownTransformer extends TextTransformer{

    @Override
    public String transform(String text){
        char[] strArr = text.toCharArray();
        StringBuilder rez = new StringBuilder();
        for (int i = 0; i < strArr.length; i++){
            if (i % 2 == 0){
                rez.append(Character.toUpperCase(strArr[i]));
            }else{
                rez.append(Character.toLowerCase(strArr[i]));
            }
        }
        return rez.toString();
    }
}
