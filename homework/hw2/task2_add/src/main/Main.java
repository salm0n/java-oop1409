package main;


import java.io.File;

public class Main {
    public static void main(String[] args) {
        String textSample = "hello";

        TextTransformer txt1 = new TextTransformer();
        System.out.println(textSample + " -> "+ txt1.transform(textSample));

        InverseTransformer txt2 = new InverseTransformer();
        System.out.println(textSample + " -> "+ txt2.transform(textSample));

        UpDownTransformer txt3 = new UpDownTransformer();
        System.out.println(textSample + " -> "+ txt3.transform(textSample));

        TextSaver text_s = new TextSaver(new UpDownTransformer(), new File("print.txt"));
        text_s.saveTextToFile(textSample);
    }
}
