package main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TextSaver{
    private TextTransformer text;
    private File file;

    public TextSaver(TextTransformer text, File file){
        super();
        this.text = text;
        this.file = file;
    }

    public TextSaver() {
        super();
    }

    public TextTransformer getText() {
        return text;
    }

    public void setText(TextTransformer text) {
        this.text = text;
    }

    public File getFile (){
        return file;
    }

    public void setFile (File file){
        this.file = file;
    }

    public void saveTextToFile (String text){
        try(PrintWriter pw = new PrintWriter(file)){
            pw.println(text);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}