package main;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File folderIn = new File("folderIn");
        File folderOut = new File("folderOut");
        try {
            System.out.println(CopyFileService.copyAllFiles(folderIn, folderOut, "docx"));
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
