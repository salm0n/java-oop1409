package main;

import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;


public class GroupFileStorage {
    public static void saveGroupToCSV(Group gr) throws IOException {
        File folder = new File("group_list");
        folder.mkdirs();
        File csvFile = new File("group_list", gr.getGroupName() + ".csv");
        CSVStringConverter csv = new CSVStringConverter();
        try(PrintWriter pw = new PrintWriter(csvFile)){
            for (Student st : gr.getStudents()){
                if (st != null) {
                    pw.println(csv.toStringRepresentation(st));
                }
            }
        }
    }

    public static Group loadGroupFromCSV(File file) throws IOException{
        CSVStringConverter csv = new CSVStringConverter();
        Group gr = new Group();
        try (Scanner sc = new Scanner(file)){
            gr.setGroupName(file.getName().substring(0, file.getName().lastIndexOf(".")));
            for (;sc.hasNextLine();){
                try {
                    gr.addStudent(csv.fromStringRepresentation(sc.nextLine()));
                }catch(GroupOverflowException e){
                    e.printStackTrace();
                }
            }
        }
        return gr;
    }

    public static File findFileByGroupName(String groupName, File workFolder){
        File[] arrFile = workFolder.listFiles();
        if (arrFile != null) {
            for (File fl : arrFile) {
                if (fl.getName().startsWith(groupName)) {
                    return fl;
                }
            }
        }
        return null;
    }
}
