package main;

public class GroupOverflowException extends Exception{

    public GroupOverflowException() {

    }

    public GroupOverflowException(String message) {
        super(message);
    }

    public GroupOverflowException(Throwable cause){
        super(cause);
    }

    public GroupOverflowException(String message, Throwable cause){
        super(message, cause);
    }


}
