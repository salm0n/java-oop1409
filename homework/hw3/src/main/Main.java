package main;

public class Main {
    public static void main(String[] args){
        Group gr1 = new Group("A-32");

        Student st1 = new Student("Harry", "Palmer", Gender.FEMALE, 3, "A-32");
        Student st2 = new Student("Mark", "Barnett", Gender.MALE, 5, "A-32");
        Student st3 = new Student("Lora", "Walters ", Gender.FEMALE, 6, "A-32");
        Student st4 = new Student("Jam", "Bryant", Gender.MALE, 7, "A-32");
        Student st5 = new Student("Dmitriy", "Jackson", Gender.MALE, 9, "A-32");
        Student st6 = new Student("Betty", "Armstrong", Gender.FEMALE, 13, "A-32");
        Student st7 = new Student("John ", "Prokopenko", Gender.MALE, 16, "A-32");
        Student st8 = new Student("Lloyd", "Brooks", Gender.MALE, 14, "A-32");
        Student st9 = new Student("Betty", "Wood", Gender.FEMALE, 11, "A-32");
        Student st10 = new Student("Antonia", "Summers", Gender.FEMALE, 1, "A-32");

        try{
            gr1.addStudent(st1);
            gr1.addStudent(st2);
            gr1.addStudent(st3);
            gr1.addStudent(st4);
            gr1.addStudent(st5);
            gr1.addStudent(st6);
            gr1.addStudent(st7);
            gr1.addStudent(st8);
            gr1.addStudent(st9);
            gr1.addStudent(st10);
        }catch(GroupOverflowException e){
            e.printStackTrace();
        }

        try{
            System.out.println(gr1.searchStudentByLastName("Minotaurus"));
        }catch (StudentNotFoundException e){
            e.printStackTrace();
        }

        System.out.println(gr1.removeStudentById(11));

        System.out.println(gr1.toString());
    }
}
