package main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputStudentData extends Student{
    public static Student input (){
        Scanner sc = new Scanner(System.in);
        System.out.print("Name: ");
        String stName = sc.nextLine();
        System.out.print("Last name: ");
        String stLastName = sc.nextLine();
        System.out.print("Gender: ");
        Gender gender;
        try {
            gender = Gender.valueOf(sc.nextLine().toUpperCase());
        }catch (IllegalArgumentException e){
            gender = Gender.OTHER;
        }
        int stId = 0;
        while (stId <= 0) {
            try {
                System.out.print("ID: ");
                stId = sc.nextInt();
                if (stId < 0){
                    System.err.println("Id can't be negative!");
                }
            } catch (InputMismatchException e) {
                sc.next();
                System.err.println("Id must be integer!");
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        sc.nextLine();
        System.out.print("Group: ");
        String stGroupName = sc.nextLine();

        return new Student(stName, stLastName, gender, stId, stGroupName);
    }
}
