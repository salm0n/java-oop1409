package main;

public class Main {
    public static void main(String[] args){

        Group group1 = new Group("A-42");

        Student st1 = InputStudentData.input();
        Student st2 = InputStudentData.input();

        try{
            group1.addStudent(st1);
            group1.addStudent(st2);
        }catch (GroupOverflowException e){
            e.printStackTrace();
        }

        try{
            System.out.println(group1.searchStudentByLastName("Minotaurus"));
        }catch (StudentNotFoundException e){
            e.printStackTrace();
        }

        System.out.println(group1.removeStudentById(11));
        System.out.println(group1.toString());

        CSVStringConverter csv = new CSVStringConverter();
        System.out.println(csv.toStringRepresentation(st1));
        System.out.println(csv.fromStringRepresentation(csv.toStringRepresentation(st1)));
    }
}
