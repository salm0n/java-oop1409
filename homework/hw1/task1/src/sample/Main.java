package sample;

public class Main {
    public static void main(String[] args) {
        Guitar guitar1 = new Guitar("Fender", 800, "red", "alder");
        Guitar guitar2 = new Guitar("Gibson", 1300, "sunburst", "mahogany");

        System.out.println(guitar1.getColor());
        guitar1.setColor("sonic blue");
        guitar1.setPrice(1100);
        System.out.println(guitar1.getColor());
        System.out.println(guitar1.getPrice());
        System.out.println(guitar2.getColor());

        System.out.println(guitar1.toString());
        System.out.println(guitar2.toString());

    }
}
