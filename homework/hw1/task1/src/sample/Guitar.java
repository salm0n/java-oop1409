package sample;

public class Guitar {
    private String brand;
    private double price;
    private String bodyMaterial;
    private String color;

    public Guitar(String brand, double price, String color, String bodyMaterial) {
        this.brand = brand;
        this.price = price;
        this.color = color;
        this.bodyMaterial = bodyMaterial;
    }

    public Guitar (){
        super();
    }

    public String getModel (){
        return brand;
    }

    public void setModel(String brand){
        this.brand = brand;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public String getBodyMaterial(){
        return bodyMaterial;
    }

    public void setBodyMaterial(String body){
        this.bodyMaterial = body;
    }

    public String getColor (){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    @Override
    public String toString(){
        return "name = " + brand + ", price = " + price + ", color = "+ color + ", body = "+ bodyMaterial;
    }
}
