package sample;

public class Main {
    public static void main(String[] args) {

        Triangle tr1 = new Triangle(3, 5, 7);
        Triangle tr2 = new Triangle(6, 5, 3);
        Triangle tr3 = new Triangle(3, 4, 6);

        System.out.println(tr1.getSideA());
        System.out.println(tr1.square());

        tr2.setSideC(11);
        System.out.println(tr2.getSideC());
        System.out.println(tr3.square());
        System.out.println(tr1.toString());
    }
}
