package sample;

public class Triangle {
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle (double sideA, double sideB, double sideC){
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public Triangle(){
        super();
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideA;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public double square (){
        double s;
        double p = (sideA + sideB + sideC) / 2;
        s = Math.sqrt((p - sideA) * (p - sideB) * (p - sideC));
        return s;
    }

    @Override
    public String toString(){
        return "sideA = " + sideA + ", sideB = " + sideB + ", sideC = " + sideC;
    }
}